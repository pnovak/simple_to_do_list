﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TDL.Models
{
    public class Event
    {

        public int EventID { get; set; }
        public string OwnerID { get; set; }
        public string Title { get; set; }
        public DateTime Deadline { get; set; }
        public string Description { get; set; }
        public bool Done { get; set; }

        public Event()
        {
            Deadline = DateTime.Now;
        }
    }
}
