#pragma checksum "C:\Users\pavel\Documents\Škola\Projects\C#_projects\AveSoftTDL\AvesomeTDL\AvesomeTDL\AvesomeTDL\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8904a338065f6ea2e98c8cb62487a98b7d8e2e38"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\pavel\Documents\Škola\Projects\C#_projects\AveSoftTDL\AvesomeTDL\AvesomeTDL\AvesomeTDL\Views\_ViewImports.cshtml"
using AvesomeTDL;

#line default
#line hidden
#line 2 "C:\Users\pavel\Documents\Škola\Projects\C#_projects\AveSoftTDL\AvesomeTDL\AvesomeTDL\AvesomeTDL\Views\_ViewImports.cshtml"
using AvesomeTDL.Models;

#line default
#line hidden
#line 1 "C:\Users\pavel\Documents\Škola\Projects\C#_projects\AveSoftTDL\AvesomeTDL\AvesomeTDL\AvesomeTDL\Views\Home\Index.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8904a338065f6ea2e98c8cb62487a98b7d8e2e38", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"344f0a8bb6032990d602f7c86c3483fc9e3c6e99", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(38, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(138, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 6 "C:\Users\pavel\Documents\Škola\Projects\C#_projects\AveSoftTDL\AvesomeTDL\AvesomeTDL\AvesomeTDL\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page - My TDL";

#line default
#line hidden
            BeginContext(194, 79, true);
            WriteLiteral("    <div class=\"content\">\r\n        <h1>Welcome to <em>My To Do List</em></h1>\r\n");
            EndContext();
#line 11 "C:\Users\pavel\Documents\Škola\Projects\C#_projects\AveSoftTDL\AvesomeTDL\AvesomeTDL\AvesomeTDL\Views\Home\Index.cshtml"
         if (SignInManager.IsSignedIn(User))
        {

#line default
#line hidden
            BeginContext(330, 50, true);
            WriteLiteral("            <a href=\"Events/Index\">My Events</a>\r\n");
            EndContext();
#line 14 "C:\Users\pavel\Documents\Škola\Projects\C#_projects\AveSoftTDL\AvesomeTDL\AvesomeTDL\AvesomeTDL\Views\Home\Index.cshtml"
        }
        else
        {

#line default
#line hidden
            BeginContext(416, 209, true);
            WriteLiteral("            <p>If you already have account</p>\r\n            <a href=\"Identity/Account/Login\">Log In</a>\r\n            <p> If don\'t ,never mind</p>\r\n            <a href=\"Identity/Account/Register\">Register</a>\r\n");
            EndContext();
#line 21 "C:\Users\pavel\Documents\Škola\Projects\C#_projects\AveSoftTDL\AvesomeTDL\AvesomeTDL\AvesomeTDL\Views\Home\Index.cshtml"
        }

#line default
#line hidden
            BeginContext(636, 14, true);
            WriteLiteral("    </div>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public UserManager<IdentityUser> UserManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public SignInManager<IdentityUser> SignInManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
